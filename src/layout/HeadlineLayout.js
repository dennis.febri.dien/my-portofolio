import React from 'react';
import {Box} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import headline from '../imgs/headline.jpg'
import {Animated} from "react-animated-css";
 

const useStyles = makeStyles(theme => ({
    banner:{
        backgroundImage: `url(${headline})`,
        backgroundSize:'cover',
        backgroundPosition:'center center',
        backgroundRepeat:'no-repeat',
        color:'#FFFFFF',
        display:'flex',
        flexDirection:'column',
        justifyContent: 'center',
        height: '100vh'
    },
    highlight:{
        backgroundColor: theme.palette.common.white,
        padding: theme.spacing(1),
        margin:'2vh',
        display:'inline-block',
    },
    textBig:{
        ...theme.typography.h1,
        color:theme.palette.text.primary,
        backgroundColor:theme.palette.background.default,
        display:'inline-block',
        fontSize:'5rem',
        textAlign:'center'
    },
    textMedium:{
        ...theme.typography.h2,
        color:theme.palette.text.primary,
        backgroundColor:theme.palette.background.default,
        display:'inline-block',
        fontSize:'3rem',
        textAlign:'center'
    },
  }));
  


export default function  HeadlineLayout() {
    const classes = useStyles();
    
    return(
        <Box >
            <div className={classes.banner}>
                <Animated animationIn="fadeInLeft" isVisible={true} className={classes.textBig}>
                    <div>
                        {"Hi!"}
                    </div>
                </Animated>
                <Animated animationIn="fadeInLeft" isVisible={true} className={classes.textBig}>
                    <div>
                        {"I'm Dennis"}
                    </div>
                </Animated>
            </div>
        </Box>
    );
}