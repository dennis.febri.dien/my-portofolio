import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { Grid } from '@material-ui/core';
import MessageEditor from './component/MessageEditor';
import MessageList from './component/MessageList';

const useStyles = makeStyles(theme => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    // marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0, 0, 0),
  },
}));

export default function FooterLayout(props) {
  const classes = useStyles();

  return (
    <div className={classes.footer} >
      
      <div ref={props.footerRef}></div>
      
      <Grid container >

        <MessageEditor />
        <MessageList />
      
      </Grid>
      <ContactInfo />
    </div>
  );
}

function ContactInfo(props) {

  return (
    <>
      <Container maxWidth="lg" style={{marginTop:'40px'}}>
        <Typography variant="h6" align="center" gutterBottom>
          {"You can contact me in below"}
        </Typography>
        <Typography variant="body1" align="center" color="textSecondary" component="p">
          {'e-mail  : dennis.febri.dien@gmail.com'}
        </Typography>
        <hr/>
        <Copyright />
      </Container>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" style={{zIndex:"-10"}}>
        <path fill="#0099ff" fillOpacity="1" d="M0,224L40,202.7C80,181,160,139,240,112C320,85,400,75,480,96C560,117,640,171,720,165.3C800,160,880,96,960,64C1040,32,1120,32,1200,48C1280,64,1360,96,1400,112L1440,128L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
      </svg>
    </>
  );
}

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Dennis Febri Dien
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}
