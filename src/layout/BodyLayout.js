import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PhotoProfile from './component/PhotoProfile'
import AboutMe from './component/AboutMe';
import ListSkills from './component/ListSkills';
import ProjectSlider from './component/ProjectSlider';
import { Box } from '@material-ui/core';

const useStylesProfile =  makeStyles(theme => ({
    bg: {
        minHeight: '100vh',
        height: 'auto',
        backgroundColor: theme.palette.text.secondary
    }
}));

const useStylesProject =  makeStyles(theme => ({
    bg: {
        minHeight: '100vh'
    }
}));

function Profile(props){
    const classes = useStylesProfile();

    return(
        <Box display="flex" justifyContent="center" flexWrap="wrap" className={classes.bg}>
            <div ref={props.aboutMeRef}></div>
            <PhotoProfile />
            <AboutMe />
            <ListSkills/>
        </Box>
    );

}

function Project(props){
    const classes = useStylesProject();

    return(
        <Box display="flex" justifyContent="flex-start" flexWrap="wrap" className={classes.bg}>
            <div ref={props.experienceRef}></div>
            <ProjectSlider />
        </Box>
    );

}


export default function BodyLayout(props) {
    
    
    return(
        <>
            <Profile aboutMeRef={props.aboutMeRef}/>
            <Project experienceRef={props.experienceRef}/>

        </>
    );
}
