import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';

import TotBio from '../../imgs/project/tot_bio_vert.png';
import Sipekan from '../../imgs/project/sipekan_vert2.png';
import Kebab from '../../imgs/project/kebab_vert.png'

import Azure from '../../imgs/logo/microsoft_azure_logo.png';
import Docker from '../../imgs/logo/docker_logo.png';
import GitlabCI from '../../imgs/logo/gitlab_ci_cd_logo.png';
import Vue from '../../imgs/logo/vue_logo.png';
import Django from '../../imgs/logo/django_logo.png';
import WebBluetooth from '../../imgs/logo/web_bluetooth_logo.png';
import ReactLogo from '../../imgs/logo/react_js_logo.png';
import PWA from '../../imgs/logo/pwa_logo.png'
import Heroku from '../../imgs/logo/heroku_logo.png'
import Haskell from '../../imgs/logo/haskell_logo.png'
import Raspberry from '../../imgs/logo/raspberry_pi_logo.png'
import Dlib from '../../imgs/logo/dlib_logo.png'
import OpenCV from '../../imgs/logo/open_cv_logo.png'
import Tensorflow from '../../imgs/logo/tf_logo.png'
import Postgre from '../../imgs/logo/postgres_logo.png'
import SQLite from '../../imgs/logo/sqlite_logo.png'
import BingMaps from '../../imgs/logo/bing_maps_logo.png'

import { Box, Button, Link } from '@material-ui/core';
import LanguageIcon from '@material-ui/icons/Language';

import Swipe from 'react-easy-swipe';

const list_project = [
  {
    nameProject : "ToT Bio",
    alias : "Toilet of Things",
    workingAs : "Head Team, Backend Developer, and IoT Engineer",
    bgColor : "#3f7162",
    textColor : "#ffffff",
    description : `I have work on disease detection by implementing smart device on toilet and communicate the device using Web Bluetooth (Based on GATT Protocol).
    This project implementing multiple unique technologies such as Machine Learning, IoT, Progressive Web App, and Embedded. System. 
    `,
    technologyUsed : [Azure, Vue, WebBluetooth, Docker, GitlabCI, Django, PWA, OpenCV, Raspberry, Postgre],
    linkToWeb : 'https://tinyurl.com/tot-bio'
  },
  {
    nameProject : "Computational Finance",
    alias : "Functional Programming on Haskell/Reactjs",
    workingAs : "DevOps and Backend Developer",
    bgColor : "#8dc2fd",
    textColor : '#ffffff',
    description : `I have implemented a computing finance website using paradigm functional programming. 
    I have work on the backend using Haskell and integrate GitLab-CI on the repository. `,
    technologyUsed : [Heroku, ReactLogo, Haskell, GitlabCI],
    linkToWeb : 'https://kebab-computational-finance.herokuapp.com/'
  },
  {
    nameProject : "Sipekan",
    alias : "Drowsiness Detection Based on Blink Length Time",
    workingAs : "Head Team, Backend Developer, and IoT and AI Engineer",
    textColor : '#ffffff',
    bgColor : "#000000",
    description : `This project has won gold medal in competition Gemastik 11.
    This application could detect drowsiness of the driver.
    I tried to implement Artificial Inteligence to detect drowsiness based on rate blink length time.
    This project is implemented on car display.
    `,
    technologyUsed : [Raspberry, Django, GitlabCI, Dlib, OpenCV, Tensorflow, SQLite, BingMaps],
    linkToWeb : 'https://youtu.be/Zlp-hJIJy8A'
  },
];

const styles = {
  bgTotBio:{
    backgroundColor: '#3f7162',
    transition: 'background-color .3s linear'
  },
  bgSipekan:{
    backgroundColor: '#1a1a1a',
    transition: 'background-color .3s linear'
  },
  bgKebab:{
    backgroundColor: '#4f6c98',
    transition: 'background-color .3s linear'
  },
  imgTotBio:{
    backgroundImage: `url(${TotBio})`,
    transition: 'background-image .3s linear'
  },
  imgSipekan:{
    backgroundImage: `url(${Sipekan})`,
    transition: 'background-image .3s linear'
  },
  imgKebab:{
    backgroundImage: `url(${Kebab})`,
    transition: 'background-image .3s linear'
  }
};


const useStyles = makeStyles(theme => ({

  container: {
    width: '100vw',
    backgroundColor: '#8dc2fd',
    
  },
  paper: {
    margin: theme.spacing(1),
  },
  fullWidth: {
    height: "auto"
  },
  banner:{
    display: 'flex',
    justifyContent: 'center',
    alignItems:'flex-end',
    margin: 0,
    backgroundImage: `url(${TotBio})`,
    opacity:1,
    backgroundSize:'cover',
    backgroundPosition:'center top',
    backgroundRepeat:'no-repeat',
    color:'#FFFFFF',
    minHeight: '100vh',
    flex: '0 50%',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '100%',
      flex: '0 100%',
      width: '100%',
      height: '70vh',
    },
  },
  round : {
    borderRadius: '50%'
  },

  arrow : {
    backgroundColor: '#f1f1f1',
    color: 'black',
  }

}));


export default function ProjectSlider(props) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(1);

  const handleChange = (inp) => {
      if(inp < 1){
        inp = 1
      } else if (inp > 4) {
        inp = 3
      }
      setChecked(inp);
  };



  return (
      
      <Swipe onSwipeLeft = { ()=>handleChange(checked-1) }
            onSwipeRight = { ()=>handleChange(checked+1) }
            tolerance = { 100 } className={classes.container}>
        <Box display={'flex'} flexWrap={'wrap'} justifyContent={'flex-start'}  
          style={{
            ...(checked === 1 ? styles.bgTotBio : {}),
            ...(checked === 2 ? styles.bgKebab : {}),
            ...(checked === 3 ? styles.bgSipekan : {}),
          }}>
          <div className={classes.banner} 
            style={{
              ...(checked === 1 ? styles.imgTotBio : {}),
              ...(checked === 2 ? styles.imgKebab : {}),
              ...(checked === 3 ? styles.imgSipekan : {}),
            }}>

            <RadioButtons checked={checked} handleChange={handleChange}/>

          </div>
          <DescriptionProject list_project={list_project} checked={checked}/>
        </Box>
      </Swipe>
  );
}

const useStylesDesc = makeStyles(theme => ({

  container:{
    margin: 0,
    color:'#FFFFFF',
    height: 'auto',
    width: '50vw',
    flex: '0 50%',
    [theme.breakpoints.down('sm')]: {
      
      flex: '0 100%',
      width: '100%',
      minHeight: '100vh',
    },
  },
  title:{
    marginTop:'2vh',
    ...theme.typography.h4,
    textAlign: 'center',
    textDecoration: 'underline'
  },
  subheading:{
    margin:'20px',
    ...theme.typography.h5,
    textDecoration: 'underline'
  },
  overline:{
    ...theme.typography.overline,
    textAlign: 'center',
    fontSize: '1.25rem'
  },

  button:{
    margin:'20px',
    color: theme.palette.common.white,
    borderWidth: "2px",
    borderColor: theme.palette.common.white,
    
  },

  textButton:{
    ...theme.typography.button,
    color: theme.palette.common.white
  },

  buttonContainer:{
    display:'flex',
    justifyContent:'center',
  },

  text:{
    ...theme.typography.body1,
    marginLeft:'20px',
    marginRight:'20px',
    textAlign: 'justify'
    
  },

  imgIcon:{
    height:'64px',
    width:'auto',
    margin:'10px 10px'
  },
  cardLogo:{
    marginTop:'20px',
    marginLeft:'20px',
    marginRight:'20px',
    backgroundColor: theme.palette.background.paper,
    display:'flex',
    justifyContent:'space-around',
    flexWrap:'wrap'
  }
}));

function GetListTech(props){
  const classes = useStylesDesc();

  return(
    <>
    {
      props.list_project[props.checked-1].technologyUsed.map(image => {
        return <img className={classes.imgIcon} key={image} src={image} alt="" />
      })
    }
    </>
  )

}

function DescriptionProject(props) {
  const classes = useStylesDesc();

  return (
    <Box className={classes.container}>
      <div className={classes.title}>
        {"My Projects"}
      </div>
      <div className={classes.subheading}>
        {"Project Name"}
      </div>
      <div className={classes.overline}>
        {props.list_project[props.checked-1].nameProject}
      </div>
      <div className={classes.overline}>
        {props.list_project[props.checked-1].alias}
      </div>
      <div className={classes.subheading}>
        {"Working as"}
      </div>
      <div className={classes.text}>
        {props.list_project[props.checked-1].workingAs}
      </div>
      <div className={classes.subheading}>
        {"Description Project :"}
      </div>
      <div className={classes.text}>
        {props.list_project[props.checked-1].description}
      </div>
      <div className={classes.subheading}>
        {"Use Technology :"}
      </div>
      <Paper className={classes.cardLogo}>
        <GetListTech checked={props.checked} list_project={props.list_project} />
      </Paper>
      <div className={classes.subheading}>
        {"Link to Web"}
      </div>
      <div className={classes.buttonContainer}>
        <Link href={props.list_project[props.checked-1].linkToWeb} variant="body2" className={classes.textButton}>
          <Button
            variant="outlined"
            size='large'
            className={classes.button}
            endIcon={<LanguageIcon/>}
          >
                {'Go to web'}
          </Button>
        </Link>
      </div>

    </Box>
  )
}


const useStylesRadioButtons = makeStyles(theme => ({

  spanButton: {
    width:"100%",
    display:'flex',
    justifyContent:'center',
    backgroundColor:'rgba(52, 52, 52, 0.5)',
    color: 'white'
  }
}));


const WhiteRadio = withStyles({
  root: {
    color: "white"
  },
  checked: {},
})(props => <Radio color="default" {...props} />);



function RadioButtons(props) {
  const classes = useStylesRadioButtons();

  return (
    <div className={classes.spanButton}>
      <WhiteRadio
        checked={props.checked === 1}
        onChange={() => props.handleChange(1)}
        value= "1"
        name="radio-button-demo"
        inputProps={{ 'data': '1' }}
      />
      <WhiteRadio
        checked={props.checked === 2}
        onChange={() => props.handleChange(2)}
        value= "2"
        name="radio-button-demo"
        inputProps={{ 'data': '2' }}
      />
      <WhiteRadio
        checked={props.checked === 3}
        onChange={() => props.handleChange(3)}
        value= "3"
        name="radio-button-demo"
        inputProps={{ 'data': '3' }}
      />
    </div>
  );
}


