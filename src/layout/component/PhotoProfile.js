import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import photoDennis from '../../imgs/photo_dennis.jpg';

const useStyles = makeStyles(theme => ({
    card: {
        
        border: '2px solid white',
        padding: '10px',
        margin: '20px',
        borderRadius: '25px',
        backgroundColor: theme.palette.background.paper,
        width: '40vh',
        height: 'auto'
    },
    imageProfile:{
        display: 'block',
        border: '2px solid white',
        padding: '10px',
        borderRadius: '25px',
        marginLeft: 'auto',
        marginRight: 'auto',
        objectFit: 'contain',
        width:'80%'
    },
    subText:{
        ...theme.typography.overline,
        backgroundColor: theme.palette.background.paper,

        fontSize: '1rem',
        textAlign: 'center'
    },
    profileText:{
        ...theme.typography.body1,
        backgroundColor: theme.palette.background.paper,

        fontSize: '1rem',
        textAlign: 'center'
    }
}));


export default function PhotoProfile() {
    const classes = useStyles();
    
    return(
        <>
            <div className={classes.card}>
                <img src={photoDennis} className={classes.imageProfile} alt={"Dennis Dien"}/>
                <div className={classes.subText}>{"Name"}</div>
                <div className={classes.profileText}>{"Dennis Febri Dien"}</div>
            </div>
        </>
    );
}
