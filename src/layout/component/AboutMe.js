import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    card: {
        border: '2px solid white',
        padding: '10px',
        margin: '20px',
        borderRadius: '25px',
        backgroundColor: theme.palette.background.paper,
        width: '100vh',
        height: 'auto'
    },
    subText:{
        ...theme.typography.overline,
        backgroundColor: theme.palette.background.paper,

        fontSize: '1rem',
        textAlign: 'center'
    },
    profileText:{
        ...theme.typography.body1,
        backgroundColor: theme.palette.background.paper,
        fontSize: '1rem',
        textAlign: 'justify',
        paddingLeft: '5vh',
        paddingRight: '4vh'
    },
    heading:{
        ...theme.typography.h2,
        backgroundColor: theme.palette.background.paper,

        fontSize: '1.7rem',
        paddingLeft: '5vh',
        paddingTop : '3vh'
    }
}));

const aboutMeContent =
`
I am Dennis, computer science undergraduate student at University of Indonesia. 
I am interested in backend development and I believe I have capability to work with team and quickly learn about framework and modules stuff.`


export default function AboutMe() {
    const classes = useStyles();
    
    return(
        <>
            <div className={classes.card}>
                <div className={classes.heading}>{"About Me"}</div>
                <div className={classes.profileText}>{aboutMeContent}</div>
            </div>
        </>
    );
}
