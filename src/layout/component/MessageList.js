import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, List, ListItem, ListItemText, Card, Grid } from '@material-ui/core';


const useStyles = makeStyles(theme => ({
    block: {
      
        width: '40vw',
        margin:'10px' 
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
      maxHeight: '80%',
      overflow: 'auto'
    },
    title: {
      margin: theme.spacing(1, 1, 1),
    },
    fulfillH: {
      height: '60vh',
      padding: '10px',
      margin: '20px'
    }
  }));
  
function handleErrors(response) {
  if (!response.ok) {
      throw Error(response.statusText);
  }
  return response;
}


const API = process.env.REACT_APP_API_URI
const GET_MESSAGE_QUERY = 'getMessage'
export default function MessageList(props){
    const classes = useStyles();
    
    const [varState, setVarState] = useState({
      hits:[],
      isError:true
    });

    useEffect(() => {


      fetch(API+GET_MESSAGE_QUERY,
        {
          method:'GET'
        }
        )
        .then(handleErrors)
        .then(response => response.json())
        .then(data => setVarState({
          hits: data,
          isError:false
        }))
        .catch(function(error) {
          console.log("Error API Called")
        });
    }, []);
    

    return(
        <Grid item xs={12} md={6}> 
          <Card className={classes.fulfillH} >
          <Typography variant="h6" className={classes.title}>
            Comment Section
          </Typography>
          <div className={classes.demo}>
            {
              varState.isError?
              <p>Error Loading</p> :
              <List dense={true}>
                <ListComment data={varState.hits} />
              </List>
            }
          </div>
          </Card>
        </Grid>
    );

}

function ListComment(props) {

  let listitem = props.data.map(datum => {
    return <ListItem key={datum._id}>
      <ListItemText
        primary={datum.name}
        secondary={datum.quote}
      />
    </ListItem>
  });
  return (
    <>
      {listitem}
    </>
  )
}