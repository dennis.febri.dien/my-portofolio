import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Card, TextField, Button, Grid, Typography } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';

const useStyles =  makeStyles(theme => ({
    block: {
        width: '40vw',
        margin: theme.spacing(1, 1, 1),
    },
    root: {
        padding: '10px'
    },
    break:{
        marginBottom: '10px'
    },
    bg: {
        minHeight: '100vh'
    },
    fulfillH: {
        height: 'auto',
        padding: '10px',
        margin: '20px'
    }
}));

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
  }

const API = process.env.REACT_APP_API_URI
const SEND_QUERY = 'send'


function handleSubmit(event){
    let query = {
        'name': event.target.nickname.value,
        'quote': event.target.message.value
    }

    let formBody = [];
    for (let property in query) {
        let encoKey = encodeURIComponent(property);
        let encoVal = encodeURIComponent(query[property]);
        formBody.push(encoKey+"="+encoVal);
    }
    formBody = formBody.join("&");

    console.log(event);
    console.log(event.target.nickname.value)
    console.log(event.target.message.value)

    fetch(API+SEND_QUERY, {
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': "application/x-www-form-urlencoded" 
        },
        body: formBody
    })
    .then(handleErrors)
    .catch(function(error) {
        console.log("Error API Called")
    });
}

export default function MessageEditor(props){
    const classes = useStyles();

    return(
        <Grid item xs={12} md={6}>
            <Card className={classes.fulfillH}>
            <form onSubmit={handleSubmit} className={classes.root} noValidate autoComplete="off" required>
                <Typography variant="h6">
                    Comment Editor
                </Typography>
                <Box display={'flex'} flexDirection={'column'}>
                    <div>
                        <TextField required id="outlined-basic" name="nickname" label="NickName" variant="outlined" />
                    </div>
                    <TextField
                        id="outlined-multiline-static"
                        label="Message"
                        multiline
                        rows="8"
                        defaultValue=""
                        variant="outlined"
                        name="message"
                        className={classes.break}
                        style={{marginTop:"20px"}}
                    />
                    <div>
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        endIcon={<Icon>send</Icon>}
                        type="submit"
                    >Send</Button>
                    </div>
                </Box>
            </form>
            </Card>
        </Grid>
    );

}