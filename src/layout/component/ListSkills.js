import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Box } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    card: {
        border: '2px solid white',
        padding: '10px',
        margin: '20px',
        borderRadius: '25px',
        backgroundColor: theme.palette.background.paper,
        width: '150vh',
        height: 'auto',
    },
    subText:{
        ...theme.typography.overline,
        backgroundColor: theme.palette.background.paper,

        fontSize: '1.1rem',
        textAlign: 'center'
    },
    profileText:{
        ...theme.typography.body1,
        backgroundColor: theme.palette.background.paper,
        fontSize: '0.7rem',
        textAlign: 'justify',
        paddingLeft: '5vh',
        paddingRight: '4vh',
    },
    heading:{
        ...theme.typography.overline,
        backgroundColor: theme.palette.background.paper,
        textAlign: 'center',
        fontSize: '1.1rem',
        paddingTop : '1vh'
    },
    imgIcon:{
        height:'64px',
        width:'auto',
        margin:'0px 10px',
        marginTop: "5px",
    }

}));

export default function ListSkills() {
    const classes = useStyles();

    let array = ["react_js_logo", "django_logo", "material_ui_logo",
                "postgres_logo", "mongo_db_logo","microsoft_azure_logo","docker_logo",
                "sonarqube_logo","node_js_logo","next_js_logo","tf_logo",
                "gitlab_ci_cd_logo","mosquitto_logo","heroku_logo","haskell_logo"];

    let images = array.map(image => {
       return <img className={classes.imgIcon} key={image} src={require(`../../imgs/logo/${image}.png`)} alt="" />
    });

    
    return(
        <>
            <div className={classes.card}>
                <div className={classes.heading}>
                    {"Has Experienced on"}
                </div>
                <Box display="flex" flexWrap="wrap" justifyContent="center">
                    {images}
                </Box>
            </div>
        </>
    );
}
