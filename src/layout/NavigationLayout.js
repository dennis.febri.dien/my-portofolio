import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import EmailIcon from '@material-ui/icons/Email';
import Switch from '@material-ui/core/Switch';
import Slide from '@material-ui/core/Slide';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles( theme =>({
    root: {
      width: 500,
    },
    stickToBottom: {
        width: '75%',
        left: '50%',
        marginLeft: '-37.5%',
        position: 'fixed',
        bottom: 0,
        zIndex: theme.zIndex.tooltip,
        backgroundColor: theme.palette.background.default
    },
    hideShow: {
        right:'2vw',
        height: '56px', // Hardcoded, based on height BottomNavigation
        position: 'fixed',
        bottom: 0,
        zIndex: theme.zIndex.tooltip + 1,
        color: theme.palette.text.secondary
    }
  }));
  

export default function  NavigationLayout(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(-1);
    const [showNav, setShowNav] = React.useState(false);

    const handleChange = () => {
        setShowNav(prev => !prev);
    };
    
    return(
        <div>
            <FormControlLabel
                control={<Switch checked={showNav} onChange={handleChange} />}
                className={classes.hideShow}
            />
            <Slide direction="up" in={showNav} mountOnEnter unmountOnExit>
                <div className={classes.stickToBottom}>
                    <BottomNavigation
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        showLabels
                    >
                        <BottomNavigationAction label="About Me" icon={<AccountCircleIcon/>} 
                            onClick={() => props.executeScroll(props.list_ref[1])}/>
                        <BottomNavigationAction label="Experience & Skill" icon={<EmojiEventsIcon />} 
                            onClick={() => props.executeScroll(props.list_ref[2])}/>
                        <BottomNavigationAction label="Contact" icon={<EmailIcon />} 
                            onClick={() => props.executeScroll(props.list_ref[0])}/>
                    </BottomNavigation>
                </div>
            </Slide>
        </div>
    );
}
