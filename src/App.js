import React, { useRef } from 'react';
import NavigationLayout from './layout/NavigationLayout';
import BodyLayout from './layout/BodyLayout';
import HeadlineLayout from './layout/HeadlineLayout';
import FooterLayout from './layout/FooterLayout';
import smoothscroll from 'smoothscroll-polyfill';
////////////////////////////////////////////////////////////////////////////////////////////////
// SCROLL
// kick off the polyfill!
smoothscroll.polyfill();

// detect support for the behavior property in ScrollOptions
const supportsNativeSmoothScroll = 'scrollBehavior' in document.documentElement.style;

const smoothScrollTo = (to, duration) => {
  const element = document.scrollingElement || document.documentElement,
      start = element.scrollTop,
      change = to - start,
      startDate = +new Date();

  // t = current time
  // b = start value
  // c = change in value
  // d = duration
  const easeInOutQuad = (t, b, c, d) => {
      t /= d/2;
      if (t < 1) return c/2*t*t + b;
      t--;
      return -c/2 * (t*(t-2) - 1) + b;
  };

  const animateScroll = _ => {
      const currentDate = +new Date();
      const currentTime = currentDate - startDate;
      element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration));
      if(currentTime < duration) {
          requestAnimationFrame(animateScroll);
      }
      else {
          element.scrollTop = to;
      }
  };
  animateScroll();
};

const scrollChecker = ref => {

  if (supportsNativeSmoothScroll) {
      scrollToRef(ref)
  } else {
      smoothScrollTo(ref.current.offsetTop, 600);
  }

};

const scrollToRef = (ref) => {
    console.log(ref.current.offsetTop)
    window.scrollTo({
    top: ref.current.offsetTop,
    left: 0,
    behavior: 'smooth'
  })
}   

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function App() {
  const footerRef = useRef(null);
  const aboutMeRef = useRef(null);
  const experienceRef = useRef(null);

  const list_ref = [footerRef, aboutMeRef, experienceRef];


  const executeScroll = (ref) => scrollChecker(ref)

  return (
    <>
        <HeadlineLayout/>
        <BodyLayout aboutMeRef={aboutMeRef} experienceRef={experienceRef}/>
        <FooterLayout footerRef={footerRef}/>
        <NavigationLayout list_ref={list_ref} executeScroll={executeScroll}/>
    </>
  );
}

export default App;
